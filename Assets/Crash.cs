using UnityEngine;
using System.Collections;

public class Crash : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)

    {
        if (collision.relativeVelocity.magnitude > 100)
            Destroy(gameObject);
    }
}