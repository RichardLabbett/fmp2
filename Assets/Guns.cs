using System;
using UnityEngine;



public class Guns : MonoBehaviour
{
    public ParticleSystem muzzleFlash;
    public ParticleSystem muzzleFlash1;

    [SerializeField]
    private Transform[] firePoints;
    [SerializeField]
    private Rigidbody projectilePrefab;
    [SerializeField]
    private float launchForce;

    public void Start()
    {
        muzzleFlash.Stop();
        muzzleFlash1.Stop();
    }

    public void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            LaunchProjectile();
            muzzleFlash.Play();
            muzzleFlash1.Play();
                


        }

        if (Input.GetButtonUp("Fire1"))
        {
            muzzleFlash.Stop();
            muzzleFlash1.Stop();
        }


    }

    private void LaunchProjectile()
    {
        foreach (var firePoint in firePoints)
        {
            var projectileInstance = Instantiate(
                projectilePrefab,
                firePoint.position,
                firePoint.rotation);

            projectileInstance.AddForce(firePoint.forward * launchForce);
        }

    }


    

    
}


