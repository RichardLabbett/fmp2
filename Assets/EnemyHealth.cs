using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{

    public float Ehealth;
    public float maxEHealth;
    public GameObject explosionPrefab;
    public GameObject healthBarUI;
    public Slider slider;

    private void Start()
    {
        Ehealth = maxEHealth;
        slider.value = calculateEHealth();

    }

    void OnCollisionEnter(Collision possibleBullet)
    {
        Debug.Log("Hit");
        
       

        if (possibleBullet.collider.tag.Equals("Bullet")) 

        {
            Debug.Log("Bullet Collision Detected");
            TakeDamage(20);
        }
    }

    void TakeDamage(int damage)
    {
        Ehealth -= damage;
    }

    private void Update()
    {
        slider.value = calculateEHealth();
        if (Ehealth < maxEHealth)
        {
            healthBarUI.SetActive(true);
        }
        if (Ehealth <= 0)
        {
            Destroy(gameObject);
            Debug.Log("Enemy Is Dead");
            Explosion();

        }
        if (Ehealth > maxEHealth)
        {
            Ehealth = maxEHealth;
        }
    }
    float calculateEHealth()
    {
        return Ehealth / maxEHealth;
    }

    void Explosion()
    {

        Instantiate(explosionPrefab, transform.position, transform.rotation);
    }


}